import React, { Component } from 'react';

import HeaderTop from './HeaderTop';
import Footer from './Footer';

import './Registration.css';

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      firstName: '',
      surname: '',
      username: '',
      password: '',
      confirmPassword: '',
    };
  }

  handleChange = event => {
    this.setState({
      firstName: event.target.value,
    });
  };

  handleSurnameChange = event => {
    this.setState({
      surname: event.target.value,
    });
  };

  handleUsernameChange = event => {
    this.setState({
      username: event.target.value,
    });
  };

  handleEmailChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handlePassword = event => {
    this.setState({
      password: event.target.value,
    });
  };

  handleConfirmPassword = event => {
    this.setState({
      confirmPassword: event.target.value,
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    var username = this.state.username;
    var email = this.state.email;
    var firstName = this.state.firstName;
    var surname = this.state.surname;
    var password = this.state.password;
    var confirmPassword = this.state.confirmPassword;

    if (password === confirmPassword) {
      window.EirCommunitiesSelfRegController.registerUser(
        firstName,
        surname,
        username,
        email,
        password,
        function(result, event) {
          if (event.statusCode === 200) {
            console.log('RESULT:', result);
            // window.location.href = result;
          } else {
            // Need to handle errors
            console.log('Result', result);
            console.log('NOPE');
          }
        },
        { escape: false, dataType: 'json', timeout: 60000 },
      );
    } else {
      console.log('passwords are not the same');
    }
  };

  render() {
    return (
      <div>
        <HeaderTop />
        <div className="container">
          <div className="row">
            <div className="col-md-12 text-center">
              <h1> Register </h1>
              <div className="col-md-8 col-md-offset-2">
                <form>
                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <label htmlFor="name" className="control-label register-text ">
                      First Name
                    </label>
                    <input
                      type="text"
                      className="form-control login-input"
                      id="first-name"
                      name="first-name"
                      onChange={this.handleChange}
                    />
                  </div>

                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <label htmlFor="name" className="control-label register-text ">
                      Second Name
                    </label>
                    <input
                      type="text"
                      className="form-control login-input"
                      id="surname"
                      name="surname"
                      onChange={this.handleSurnameChange}
                    />
                  </div>

                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <label htmlFor="name" className="control-label register-text ">
                      Username
                    </label>
                    <input
                      type="text"
                      className="form-control login-input"
                      id="username"
                      name="username"
                      onChange={this.handleUsernameChange}
                    />
                  </div>

                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <label htmlFor="name" className="control-label register-text ">
                      Email Address
                    </label>
                    <input
                      type="text"
                      className="form-control login-input"
                      id="email"
                      name="email"
                      onChange={this.handleEmailChange}
                    />
                  </div>
                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <label htmlFor="name" className="control-label register-text ">
                      Password
                    </label>
                    <input
                      type="password"
                      className="form-control login-input"
                      id="password"
                      name="password"
                      onChange={this.handlePassword}
                    />
                  </div>
                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <label htmlFor="name" className="control-label register-text ">
                      Confirm Password
                    </label>
                    <input
                      type="password"
                      className="form-control login-input"
                      id="confirm-password"
                      name="confirm-password"
                      onChange={this.handleConfirmPassword}
                    />
                  </div>
                  <div className="form-group col-xs-12 col-md-12 text-left">
                    <a
                      href=""
                      className="button--primary btn-primary button register-button"
                      onClick={this.handleSubmit}
                    >
                      {' '}
                      Submit
                    </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Registration;
