import React, { Component } from 'react';

import './ServiceStatus.css';

class ServiceStatus extends Component {
  render() {
    return (
      <div className="container service-status-panel">
        <div className="row">
          <div className="col-md-8" />
          <div className="col-md-4">
            <span className="service-status pull-right">All services operating normally</span>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceStatus;
