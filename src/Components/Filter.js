import React, { Component } from 'react';

class Filter extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="filter-wrapper">
          <div className="form-group">
            <label htmlFor="name" className="col-lg-1" />
            <div className="col-lg-11">
              <select type="text" className="form-control filter-input" name="name" id="name">
                <option value="volvo">Filter & Sort</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Filter;
