import React, { Component } from 'react';

import './SocialLinks.css';

class SocialLinks extends Component {
  render() {
    return (
      <div className="container">
        <div className="row social-links">
          <div className="col-md-12">
            <ul className="social-links-menu">
              <li>
                <a href="">
                  <img
                    src="https://business.eir.ie/assets/img/social-icons-reloaded/facebook-white.png"
                    alt="facebook"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://business.eir.ie/assets/img/social-icons-reloaded/linkedin-white.png"
                    alt="facebook"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://business.eir.ie/assets/img/social-icons-reloaded/twitter-white.png"
                    alt="facebook"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://business.eir.ie/assets/img/social-icons-reloaded/blog-white.png"
                    alt="facebook"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://business.eir.ie/assets/img/social-icons-reloaded/youtube-white.png"
                    alt="facebook"
                  />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default SocialLinks;
