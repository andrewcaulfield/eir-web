import React, { Component } from 'react';

import './Phone.css';

class Phone extends Component {
  formatPrice(cents) {
    return `€${(cents / 100).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`;
  }
  render() {
    const { details } = this.props;
    return (
      <div className="col-xs-12 col-sm-6 col-md-3">
        <div className="phone-panel">
          <div className="phone-details">
            <div className="row">
              <div className="col-xs-6 col-sm-12 col-md-12">
                <img src={details.image} alt={details.phone} />
              </div>

              <div className="col-xs-6 col-sm-12 col-md-12">
                <div className="phone-title">{details.name}</div>
                <div className="phone-title">
                  from <span className="price">{this.formatPrice(details.price)} </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Phone;
