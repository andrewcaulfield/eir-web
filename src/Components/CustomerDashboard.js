import React, { Component } from 'react';

import CustomerHeader from './CustomerHeader';

import Orders from './Orders';
import ServiceRequests from './ServiceRequests';
import AccountBilling from './AccountBilling';
import Recommended from './Recommended';

import ServiceStatus from './ServiceStatus';

import CustomerDashboardFooter from './CustomerDashboardFooter';

class CustomerDashboard extends Component {
  render() {
    return (
      <div>
        <CustomerHeader />
        <ServiceStatus />

        <div className="container">
          <div className="row">
            <div className="col-md-7">
              <Orders />
            </div>
            <div className="col-md-5">
              <ServiceRequests />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <AccountBilling />
            </div>
          </div>
        </div>
        <Recommended />
        <CustomerDashboardFooter />
      </div>
    );
  }
}

export default CustomerDashboard;
