module.exports = {
  phone1: {
    name: 'Samsung 8',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/samsung_galaxy_s8.png',
    size: 'Everyones favorite white phone. We will cut it to the size you need and ship it.',
    price: 1724,
    status: 'available',
  },

  phone2: {
    name: 'iPhone 8',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/apple_iphone_8_plus.png',
    size: 'These tender, mouth-watering beauties are a fantastic hit at any dinner party.',
    price: 3200,
    status: 'available',
  },

  phone3: {
    name: 'Samsung Note',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/samsung_galaxy_s8.png',
    size:
      'Big, sweet and tender. True dry-pack scallops from the icey waters of Alaska. About 8-10 per pound',
    price: 1684,
    status: 'unavailable',
  },

  phone4: {
    name: 'iPhone 8 Plus',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/apple_iphone_8_plus.png',
    size:
      'Lean flesh with a mild, sweet flavor profile, moderately firm texture and large, moist flakes. ',
    price: 1129,
    status: 'available',
  },

  phone5: {
    name: 'Nokia 3',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/nokia_3.png',
    size: 'Crack these open and enjoy them plain or with one of our cocktail sauces',
    price: 4234,
    status: 'available',
  },

  phone6: {
    name: 'Sony Experia',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/sony_xperia_l1.png',
    size:
      'This flaky, oily salmon is truly the king of the sea. Bake it, grill it, broil it...as good as it gets!',
    price: 1453,
    status: 'available',
  },

  phone7: {
    name: 'HTC U11',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/htc_u11.png',
    size: 'A soft plump oyster with a sweet salty flavor and a clean finish.',
    price: 2543,
    status: 'available',
  },

  phone8: {
    name: 'Huawei',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/apple_iphone_8_plus.png',
    size: 'The best mussels from the Pacific Northwest with a full-flavored and complex taste.',
    price: 425,
    status: 'available',
  },

  phone9: {
    name: 'Alcatel',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/alcatel_u5_4g.png',
    size:
      'With 21-25 two bite prawns in each pound, these sweet morsels are perfect for shish-kabobs.',
    price: 2250,
    status: 'available',
  },
};
