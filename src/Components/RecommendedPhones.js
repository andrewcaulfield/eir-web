import React, { Component } from 'react';

import Phone from './Phone';

import './RecommendedPhones.css';

class RecommendedPhones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phones: require('./RecommendedPhoneList'),
    };
  }
  render() {
    return (
      <div>
        <div className="container-fluid">
          <div className="recommended-phones">
            <div className="row">
              <div className="col-md-12 text-center">
                <h1>Phones We Recommend</h1>
              </div>
              <div className="container">
                <div className="row">
                  {Object.keys(this.state.phones).map(key => (
                    <Phone key={key} details={this.state.phones[key]} />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RecommendedPhones;
