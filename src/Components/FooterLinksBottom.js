import React, { Component } from 'react';

import './FooterLinksBottom.css';

class FooterLinksBottom extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ul className="footer-links-bottom">
              <li>
                <a href="">Abuse reports</a>
              </li>
              <li>
                <a href="">Terms & Conditions </a>
              </li>
              <li>
                <a href="">Pricing</a>
              </li>
              <li>
                <a href="">Privacy Policy</a>
              </li>
              <li>
                <a href="">Accessibility Services</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default FooterLinksBottom;
