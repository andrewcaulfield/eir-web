import React, { Component } from 'react';

import './CustomerHeader.css';

import logo from '../eir-business-logo.png';

import bwgLogo from '../bwg-logo.png';

class CustomerHeader extends Component {
  render() {
    return (
      <div>
        <header>
          <div className="top-navigation overnav">
            <div className="container" />
          </div>

          <div className="customer-secondary-navigation">
            <div className="container">
              <nav className="navbar">
                <div className="navbar-header">
                  <button
                    type="button"
                    className="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#main-nav"
                    aria-expanded="false"
                    aria-controls="main-nav"
                  >
                    <span className="sr-only">Toggle navigation</span>

                    <span className="glyphicon glyphicon-menu-hamburger" />
                  </button>

                  <a href="/" title="St-Patricks" className="navbar-brand">
                    <img className="brand-logo img img-responsive" alt="Eir Business" src={logo} />
                  </a>
                  <a href="/" title="St-Patricks" className="navbar-brand">
                    <img
                      className="brand-logo brand-logo-2 img img-responsive"
                      alt="Eir Business"
                      src={bwgLogo}
                    />
                  </a>
                </div>

                <nav className="navbar-collapse collapse" id="main-nav">
                  <div className="customer-secondary-navigation-container container-fluid">
                    <ul className="nav navbar-nav secondary-ul">
                      <li className="">
                        <a href="">Orders</a>
                      </li>
                      <li className="">
                        <a href="">Service requests</a>
                      </li>
                      <li className="">
                        <a href="">Account billing</a>
                      </li>
                      <li className="">
                        <a href="">Network centre</a>
                      </li>
                    </ul>
                  </div>

                  <div className="primary-nav-container container-fluid" />

                  <ul className="nav navbar-nav top">
                    <li className="primary-nav-li">
                      <a href="">eir home</a>
                    </li>
                    <li className="primary-nav-li">
                      <a href="" className="active">
                        eir business
                      </a>
                    </li>
                  </ul>

                  <ul className="nav navbar-nav  nav-right">
                    <li className="primary-nav-li">
                      <a href="">Jim Boland</a>
                    </li>
                  </ul>
                </nav>
              </nav>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default CustomerHeader;
