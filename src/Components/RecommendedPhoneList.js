module.exports = {
  phone1: {
    name: 'Samsung 8',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/samsung_galaxy_s8.png',
    size: 'Everyones favorite white phone. We will cut it to the size you need and ship it.',
    price: 1724,
    status: 'available',
  },

  phone2: {
    name: 'iPhone 8',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/apple_iphone_8_plus.png',
    size: 'These tender, mouth-watering beauties are a fantastic hit at any dinner party.',
    price: 3200,
    status: 'available',
  },

  phone3: {
    name: 'Samsung Note',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/samsung_galaxy_s8.png',
    size:
      'Big, sweet and tender. True dry-pack scallops from the icey waters of Alaska. About 8-10 per pound',
    price: 1684,
    status: 'unavailable',
  },

  phone4: {
    name: 'iPhone 8 Plus',
    image:
      'https://www.eir.ie/opencms/export/sites/default/.galleries/eir/handset-images/apple_iphone_8_plus.png',
    size:
      'Lean flesh with a mild, sweet flavor profile, moderately firm texture and large, moist flakes. ',
    price: 1129,
    status: 'available',
  },
};
