import React, { Component } from 'react';

class PhoneHeader extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid">
          <div className="phone-header">
            <div className="phone-header-wrapper">
              <div className="row text-center">
                <div className="col-md-12">
                  <h1 className="phone-header-title"> Choose your perfect mobile </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PhoneHeader;
