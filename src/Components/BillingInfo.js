import React, { Component } from 'react';

import Warning from '../warning.png';

class BillingInfo extends Component {
  render() {
    return (
      <div className="billing-info">
        <p>
          <img src={Warning} className="warning" alt="warning" />1 user is due an upgrade to their
          handset
          <a href="" className="more-info">
            More info
          </a>
        </p>
      </div>
    );
  }
}

export default BillingInfo;
