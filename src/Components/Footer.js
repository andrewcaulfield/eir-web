import React, { Component } from 'react';

import './Footer.css';

import SocialLinks from './SocialLinks';
import FooterLinksBottom from './FooterLinksBottom';
import Copyright from './Copyright';

class Footer extends Component {
  render() {
    return (
      <footer>
        <div className="container">
          <div className="row buffer-top">
            <div className="col-xs-6 col-sm-4 col-md-3">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    eir. let’s make possible…
                  </a>
                </li>
                <li>
                  <a href="">Digital Boost</a>
                </li>
                <li>
                  <a href="">Starting a Business</a>
                </li>
                <li>
                  <a href="">eir Affinity Program</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Segments
                  </a>
                </li>
                <li>
                  <a href="">SME Sector</a>
                </li>
                <li>
                  <a href="">Corporate Sector</a>
                </li>
                <li>
                  <a href="">Public Sector</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Company
                  </a>
                </li>
                <li>
                  <a href="">Why eir Business</a>
                </li>
                <li>
                  <a href="">eir Group</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Business Insights
                  </a>
                </li>
                <li>
                  <a href="">Read our Blogs</a>
                </li>
                <li>
                  <a href="">Read our Case Studies</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-3">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Support
                  </a>
                </li>
                <li>
                  <a href="">Help Centre</a>
                </li>
                <li>
                  <a href="">Contact us</a>
                </li>
                <li>
                  <a href="">Billing</a>
                </li>
                <li>
                  <a href="">Find your nearest store</a>
                </li>
                <li>
                  <a href="">Sitemap</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <SocialLinks />
        <FooterLinksBottom />
        <Copyright />
      </footer>
    );
  }
}

export default Footer;
