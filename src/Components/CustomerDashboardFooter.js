import React, { Component } from 'react';

import SocialLinks from './SocialLinks';
import FooterLinksBottom from './FooterLinksBottom';
import Copyright from './Copyright';

class CustomerDashboardFooter extends Component {
  render() {
    return (
      <footer>
        <div className="container">
          <div className="row buffer-top">
            <div className="col-md-1" />
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Our Products
                  </a>
                </li>
                <li>
                  <a href="">Bundles</a>
                </li>
                <li>
                  <a href="">Broadband</a>
                </li>
                <li>
                  <a href="">TV</a>
                </li>
                <li>
                  <a href="">Mobile</a>
                </li>
                <li>
                  <a href="">Phone</a>
                </li>
                <li>
                  <a href="">Sport</a>
                </li>
                <li>
                  <a href="">eir Security</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Support
                  </a>
                </li>
                <li>
                  <a href="">Broadband Support</a>
                </li>
                <li>
                  <a href="">Phone Support</a>
                </li>
                <li>
                  <a href="">TV Support</a>
                </li>
                <li>
                  <a href="">Mobile Support</a>
                </li>
                <li>
                  <a href="">Sport Support</a>
                </li>
                <li>
                  <a href="">Smartphone Help</a>
                </li>
                <li>
                  <a href="">Billing</a>
                </li>
                <li>
                  <a href="">My eir</a>
                </li>
                <li>
                  <a href="">Report a Fault</a>
                </li>
                <li>
                  <a href="">Online Safety</a>
                </li>
                <li>
                  <a href="">Complaints</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Our Websites
                  </a>
                </li>
                <li>
                  <a href="">eir at Home</a>
                </li>
                <li>
                  <a href="">eir Business</a>
                </li>
                <li>
                  <a href="">Sport</a>
                </li>
                <li>
                  <a href="">Open eir</a>
                </li>
                <li>
                  <a href="">eir Support</a>
                </li>
                <li>
                  <a href="">eir Studyhub</a>
                </li>
                <li>
                  <a href="">Broadband Speed Test</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    eir Group
                  </a>
                </li>
                <li>
                  <a href="">News & Press</a>
                </li>
                <li>
                  <a href="">About Us</a>
                </li>
                <li>
                  <a href="">Investor Relations</a>
                </li>
                <li>
                  <a href="">Corporate Responsibility</a>
                </li>
                <li>
                  <a href="">Pricing</a>
                </li>
                <li>
                  <a href="">Careers</a>
                </li>
                <li>
                  <a href="">Directory Enquiries</a>
                </li>
                <li>
                  <a href="">Regulatory Information</a>
                </li>
                <li>
                  <a href="">Credit Insights</a>
                </li>
              </ul>
            </div>
            <div className="col-xs-6 col-sm-4 col-md-3">
              <ul className="menu">
                <li className="list-first">
                  <a href="" className="footer-first">
                    Join the conversation
                  </a>
                </li>
                <li>
                  <a href="">eir Community</a>
                </li>
                <li>
                  <a href="">Blog</a>
                </li>
                <li>
                  <a href="">Contact Us</a>
                </li>
                <li>
                  <a href="">Find a store</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <SocialLinks />
        <FooterLinksBottom />
        <Copyright />
      </footer>
    );
  }
}

export default CustomerDashboardFooter;
