import React, { Component } from 'react';

import './ServiceRequests.css';

import ServiceRequest from './ServiceRequest';

class ServiceRequests extends Component {
  render() {
    return (
      <div>
        <div className="service-requests-panel">
          <div className="row is-flex">
            <div className="col-md-8">
              <h2>Service Requests </h2>
            </div>
            <div className="col-md-4 no-padding">
              <a href="" className="service-request-button">
                New request
              </a>
            </div>
          </div>
          <div className="row is-flex">
            <ServiceRequest status="open" />
          </div>
          <div className="row is-flex">
            <ServiceRequest status="closed" />
          </div>
          <div className="row is-flex">
            <ServiceRequest status="open" />
          </div>
          <div className="row is-flex">
            <ServiceRequest status="closed" />
          </div>
          <div className="row">
            <div className="col-md-12">
              <a href="" className="more-info">
                Show more
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceRequests;
