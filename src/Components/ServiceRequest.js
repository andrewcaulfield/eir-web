import React, { Component } from 'react';

class ServiceRequest extends Component {
  render() {
    let status;
    if (this.props.status === 'open') {
      status = <div className="service-request-status open">Open</div>;
    } else {
      status = <div className="service-request-status closed">Complete</div>;
    }
    return (
      <div className="container service-request">
        <div className="row is-flex">
          <div className="col-xs-12 col-sm-12 col-md-12">Request #315535:</div>
          <div className="col-xs-12 col-sm-7 col-md-7">Tibique voluptaria intellegebat</div>
          <div className="col-xs-12 col-sm-5 col-md-5">{status}</div>
          <div className="col-xs-12 col-md-12 service-border" />
        </div>
      </div>
    );
  }
}

export default ServiceRequest;
