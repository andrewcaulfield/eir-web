import React, { Component } from 'react';

import './Orders.css';

import Order from './Order';

import OrderFilter from './OrderFilter';

class Orders extends Component {
  render() {
    return (
      <div>
        <div className="orders-panel">
          <div className="row is-flex">
            <div className="col-xs-12 col-sm-8 col-md-8">
              <h2>Orders </h2>
            </div>
            <div className="col-xs-12 col-sm-4 col-md-4">
              <a href="" className="service-request-button">
                New Order
              </a>
            </div>
          </div>
          <div className="row is-flex">
            <div className="col-md-12">
              <OrderFilter />
            </div>
          </div>
          <div className="row is-flex">
            <Order title="Hosted voice installation" date="on-time" />
          </div>
          <div className="row is-flex">
            <Order title="Port 5 new handsets" date="delayed" />
          </div>
          <div className="row is-flex">
            <Order title="SIP trunking" date="on-time" />
          </div>
          <a href="" className="more-info">
            Show more
          </a>
        </div>
      </div>
    );
  }
}

export default Orders;
