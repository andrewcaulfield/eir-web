import React, { Component } from 'react';

import './AccountBilling.css';

import Graph from '../graph.png';

import BillingInfo from './BillingInfo';

class AccountBilling extends Component {
  render() {
    return (
      <div>
        <div className="account-billing-panel">
          <div className="row is-flex">
            <div className="col-md-12">
              <h2>Account Billing </h2>
            </div>
          </div>
          <div className="row is-flex">
            <div className="col-md-8">
              <img src={Graph} className="graph-image img img-responsive" alt="graph" />
            </div>
            <div className="col-md-4">
              <h1>
                €11,241.42 <span className="month">this month</span>
              </h1>
              <p>
                <span className="increase">+132.54</span> from last month{' '}
                <a href="" className="more-info">
                  why?{' '}
                </a>
              </p>
              <div className="col-md-12 no-padding">
                <BillingInfo />
              </div>
              <div className="col-md-12 no-padding">
                <BillingInfo />
              </div>
              <a href="" className="more-info">
                Show more
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AccountBilling;
