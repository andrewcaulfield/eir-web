import React, { Component } from 'react';

import iPhone from '../iphone8.png';

class Product extends Component {
  render() {
    return (
      <div className="product-block">
        <img src={iPhone} className="product-image" alt="phone" />
        <div className="col-md-12 product-description">
          <p>
            Usu an augue persius denique, dicam corpora pertinax nam at. Ius tota melius consulatu
            in, pro facer novum ei.{' '}
          </p>
          <a href=" " className="more-info">
            More info
          </a>
        </div>
      </div>
    );
  }
}

export default Product;
