// src/routes.js
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import App from '../App';
import Registration from './Registration';
import PhoneList from './PhoneList';
import CustomerDashboard from './CustomerDashboard';

import Test from './Test';

const Routes = props => (
  <BrowserRouter>
    <Switch>
      <Route path="/test" component={Test} />
      <Route path="/dashboard" component={CustomerDashboard} />
      <Route path="/phones" component={PhoneList} />
      <Route path="/eirbusiness/EirCommunitiesSelfReg" component={Registration} />
      <Route path="/" component={App} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
