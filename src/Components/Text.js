import React, { Component } from 'react';

import '../App.css';

import $ from 'jquery';

window.jQuery = window.$ = $;

class Text extends Component {
  render() {
    return (
      <div>
        <p className="text">
          eir Vision customers can also use their my eir login to access eir Vision Go and On Demand
          services. Sky TV customers can also add eir Sports to their Sky box through my eir.
        </p>
      </div>
    );
  }
}

export default Text;
