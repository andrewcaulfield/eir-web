import React, { Component } from 'react';

class OrderFilter extends Component {
  render() {
    return (
      <div>
        <ul className="order-filter">
          <li className="">
            <a href="" className="active">
              In progress
            </a>
          </li>
          <li>
            <a href="">Fulfilled</a>
          </li>
          <li>
            <a href="">Live</a>
          </li>
        </ul>
      </div>
    );
  }
}

export default OrderFilter;
