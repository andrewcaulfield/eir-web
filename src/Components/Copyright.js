import React, { Component } from 'react';

import './Copyright.css';

class Copyright extends Component {
  render() {
    return (
      <div className="copyright-container container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <p className="copyright">
              eir is a trading name of eircom Limited, Registered as a Branch in Ireland 907674,
              Incorporated in Jersey Number 116389. Branch Address: 1 Heuston South Quarter, St.
              John's Road, Dublin 8. © 2017 All rights reserved.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Copyright;
