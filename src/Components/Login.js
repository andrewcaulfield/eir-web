import React, { Component } from 'react';

import './Login.css';

import $ from 'jquery';

import NotRegistered from './NotRegistered';
import TextSection from './Text';

window.jQuery = window.$ = $;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  handleClick = event => {
    event.preventDefault();
    var name = 'npatel2@deloitte.ie.ear.com';
    var password = 'Login12*';
    this._handleLogin(name, password);
  };

  _handleLogin(name, password) {
    window.EirCommunitiesLoginController.login(
      name,
      password,
      function(result, event) {
        if (event.statusCode === 200) {
          console.log('RESULT:', result);
          window.location.href = result;
        } else {
          console.log('Result', result);
          console.log('NOPE');
        }
      },
      { escape: false, dataType: 'json', timeout: 60000 },
    );
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-7">
              <div className="login-panel-container">
                <div className="login-panel">
                  <h2 className="text-left"> Login </h2>
                  <form>
                    <div className="form-group col-xs-12 col-md-12 text-left">
                      <label htmlFor="name" className="control-label form-text ">
                        Email Address
                      </label>
                      <input
                        type="text"
                        className="form-control login-input"
                        id="email"
                        name="email"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group col-xs-12 col-md-12 text-left">
                      <label htmlFor="name" className="control-label form-text ">
                        Password
                      </label>
                      <input
                        type="password"
                        className="form-control login-input"
                        id="password"
                        name="password"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group col-xs-12 col-md-12 text-left">
                      <a href="" className="forotten-email form-text link-hover">
                        I've forgotten my email address
                      </a>
                    </div>

                    <a
                      href=""
                      className="button--primary btn-primary button"
                      onClick={this.handleClick}
                    >
                      {' '}
                      Next
                    </a>
                    <div className="form-group col-xs-12 col-md-12 text-left">
                      <p className="form-text">
                        {' '}
                        Having trouble loggin in?{' '}
                        <a href="" className="form-text link-hover">
                          Read our FAQ
                        </a>
                      </p>
                    </div>
                  </form>
                </div>
              </div>

              <TextSection />
            </div>
            <div className="col-md-5">
              <NotRegistered />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
