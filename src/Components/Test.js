import React, { Component } from 'react';

import HeaderTop from './HeaderTop';
import Footer from './Footer';

class Test extends Component {
  render() {
    return (
      <div>
        <HeaderTop />
        <h1> In Test </h1>
        <Footer />
      </div>
    );
  }
}

export default Test;
