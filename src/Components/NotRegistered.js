import React, { Component } from 'react';

import '../App.css';

import $ from 'jquery';

window.jQuery = window.$ = $;

class NotRegistered extends Component {
  render() {
    return (
      <div>
        <div className="not-registered-container">
          <h2 className="not-registered-title">Not yet registered?</h2>

          <a
            href="/eirbusiness/EirCommunitiesSelfReg"
            className="button--primary btn-primary not-registered-button button"
          >
            Register Now
          </a>
        </div>
      </div>
    );
  }
}

export default NotRegistered;
