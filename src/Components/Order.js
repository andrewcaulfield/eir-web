import React, { Component } from 'react';

import './Order.css';

class Order extends Component {
  render() {
    let date;
    if (this.props.date === 'delayed') {
      date = (
        <span className="order-delivery delayed">
          Est. delivery date: <span className="date">3 Oct</span>
        </span>
      );
    } else {
      date = (
        <span className="order-delivery">
          Est. delivery date: <span className="date">3 Oct</span>
        </span>
      );
    }
    return (
      <div className="container order-single">
        <div className="row is-flex">
          <div className="col-xs-12 col-sm-7 col-md-2">
            <div className="circle-date">
              <div className="circle">26 SEP</div>
            </div>
            <div className="order-border" />
          </div>
          <div className="col-md-4">
            {this.props.title}{' '}
            <a href="" className="more-info">
              View order summary
            </a>
          </div>
          <div className="col-xs-12 col-sm-5 col-md-4 col-md-offset-2">{date}</div>
        </div>
      </div>
    );
  }
}

export default Order;
