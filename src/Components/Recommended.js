import React, { Component } from 'react';

import './Recommended.css';

import Product from './Product';
import News from './News';

class Recommended extends Component {
  render() {
    return (
      <div className="container-fluid recommended-block">
        <div className="recommended-panel">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h2>Based on your usage</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8 product-section">
                <div className="col-md-4">
                  <Product />
                </div>
                <div className="col-md-4">
                  <Product />
                </div>
                <div className="col-md-4">
                  <Product />
                </div>
              </div>
              <div className="col-md-4">
                <div className="col-md-12">
                  <h2> News </h2>
                  <News />
                </div>
                <div className="col-md-12">
                  <News />
                </div>
                <div className="col-md-12">
                  <News />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Recommended;
