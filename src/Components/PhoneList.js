import React, { Component } from 'react';

import Phone from './Phone';
import Header from './HeaderTop';

import Footer from './Footer';

import PhoneHeader from './PhoneHeader';
import Filter from './Filter';

import RecommendedPhones from './RecommendedPhones';

class PhoneList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phones: require('./Phones'),
    };
  }

  render() {
    return (
      <div>
        <Header />
        <PhoneHeader />
        <Filter />
        <RecommendedPhones />
        <div className="phone-list-background">
          <div className="container">
            <div className="row">
              {Object.keys(this.state.phones).map(key => (
                <Phone key={key} details={this.state.phones[key]} />
              ))}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default PhoneList;
