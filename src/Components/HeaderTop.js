import React, { Component } from 'react';

import '../App.css';

import logo from '../eir-business-logo.png';

import $ from 'jquery';

window.jQuery = window.$ = $;
require('bootstrap');

class HeaderTop extends Component {
  render() {
    return (
      <div>
        <header>
          <div className="top-navigation overnav">
            <div className="container" />
          </div>

          <div className="secondary-navigation">
            <div className="container">
              <nav className="navbar">
                <div className="navbar-header">
                  <button
                    type="button"
                    className="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#main-nav"
                    aria-expanded="false"
                    aria-controls="main-nav"
                  >
                    <span className="sr-only">Toggle navigation</span>

                    <span className="glyphicon glyphicon-menu-hamburger" />
                  </button>

                  <a href="/" title="St-Patricks" className="navbar-brand">
                    <img className="brand-logo" alt="Eir Business" src={logo} />
                  </a>
                </div>

                <nav className="navbar-collapse collapse" id="main-nav">
                  <div className="secondary-navigation-container container-fluid">
                    <ul className="nav navbar-nav second-list">
                      <li className="dropdown">
                        <a
                          href=""
                          className="dropdown-toggle"
                          data-toggle="dropdown"
                          role="button"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Login
                        </a>
                      </li>
                      <li className="dropdown">
                        <a
                          href=""
                          className="dropdown-toggle"
                          data-toggle="dropdown"
                          role="button"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Register
                        </a>
                      </li>
                      <li className="dropdown">
                        <a
                          href=""
                          className="dropdown-toggle"
                          data-toggle="dropdown"
                          role="button"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Support
                        </a>
                      </li>
                    </ul>
                  </div>

                  <div className="primary-nav-container container-fluid" />

                  <ul className="nav navbar-nav top">
                    <li className="primary-nav-li">
                      <a href="">eir home</a>
                    </li>
                    <li className="primary-nav-li">
                      <a href="">eir business</a>
                    </li>
                  </ul>
                </nav>
              </nav>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default HeaderTop;
