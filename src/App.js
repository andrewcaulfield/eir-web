import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

import HeaderTop from './Components/HeaderTop';
import Login from './Components/Login';
import Footer from './Components/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HeaderTop />
        <Login />
        <Footer />
      </div>
    );
  }
}

export default App;
